def fibonacci(n, cache = {}):
    if n == 0:
        cache[0] = 0
        return cache[0]
    if n == 1:
        cache[1] = 1
        return cache[1]
    if n not in cache:
        cache[n - 2] = fibonacci(n - 2, cache)
        cache[n - 1] = fibonacci(n - 1, cache)
        cache[n] = cache[n - 1] + cache[n - 2]
    return cache[n]


n = int(input())
print(fibonacci(n))
