#!/bin/python3

import sys

def make_change(coins, n, cache = {}):
    if n == 0:
        return 1
    if n < 0:
        return 0
    if len(coins) == 0:
        return 0

    cset = frozenset(coins)
    if cset not in cache:
        cache[cset] = {}
    m = n - coins[0]
    if m not in cache[cset]:
        cache[cset][m] = make_change(coins, n - coins[0], cache)

    tail = coins[1:]
    ctail = frozenset(tail)
    if ctail not in cache:
        cache[ctail] = {}
    if n not in cache[ctail]:
        cache[ctail][n] = make_change(tail, n, cache)
    return cache[ctail][n] + cache[cset][m]


n,m = input().strip().split(' ')
n,m = [int(n),int(m)]
coins = [int(coins_temp) for coins_temp in input().strip().split(' ')]
print(make_change(coins, n))
