def count(k, cache = {}):
    if k < 0:
        return 0
    if k == 0:
        if k not in cache:
            cache[0] = 1
        return cache[0]
    if k - 3 not in cache:
        cache[k - 3] = count(k - 3, cache)
    if k - 2 not in cache:
        cache[k - 2] = count(k - 2, cache)
    if k - 1 not in cache:
        cache[k - 1] = count(k - 1, cache)
    cache[k] = cache[k - 1] + cache[k - 2] + cache[k - 3]
    return cache[k]

s = int(input().strip())
for a0 in range(s):
    n = int(input().strip())
    print(count(n))
