from math import sqrt
def is_prime(n):
    if n == 1:
        return False
    for k in range(2, int(sqrt(n))):
        if n % k == 0:
            return False
    return True

def is_prime_opt(n):
    if n == 2:
        return True
    if n == 1 or (n & 1) == 0:
        return False
    for k in range(3, int(sqrt(n)), 2):
        if n % k == 0:
            return False
    return True

p = int(input().strip())
for a0 in range(p):
    n = int(input().strip())
    if is_prime_opt(n):
        print('Prime')
    else:
        print('Not prime')
