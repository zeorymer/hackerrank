#!/bin/python3

import sys

def make_change(coins, n, idx, cache = {}):
    if n == 0:
        return 1
    if idx >= len(coins):
        return 0
    s = 0
    c = 0
    key = '{0}-{1}'.format(n, idx)
    if key in cache:
        return cache[key]
    while s <= n:
        r = n - s
        c += make_change(coins, r, idx + 1, cache)
        s += coins[idx]
    cache[key] = c
    return cache[key]

n,m = input().strip().split(' ')
n,m = [int(n),int(m)]
coins = [int(coins_temp) for coins_temp in input().strip().split(' ')]
print(make_change(coins, n, 0))
