#!/bin/python3

import sys

def lonely_integer(a):
    bs = 0
    for i in a:
        bs ^= (1 << i)
    n = 0
    while bs != 0:
        if bs & 1 != 0:
            print(n)
        bs = bs >> 1
        n += 1


n = int(input().strip())
a = [int(a_temp) for a_temp in input().strip().split(' ')]
lonely_integer(a)
