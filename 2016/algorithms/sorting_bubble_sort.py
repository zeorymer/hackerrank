n = int(input().strip())
a = list(map(int, input().strip().split(' ')))

total = 0
for i in range(len(a)):
    nswaps = 0
    for j in range(len(a) - 1):
        if (a[j] > a[j + 1]):
            a[j], a[j + 1] = a[j + 1], a[j]
            nswaps += 1

    if nswaps == 0:
        break

    total += nswaps

print("Array is sorted in {0} swaps.".format(total))
print("First Element: {0}".format(a[0]))
print("Last Element: {0}".format(a[-1]))
