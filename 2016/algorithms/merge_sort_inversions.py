def merge_sort(arr):
    n = len(arr)
    if n < 2:
        return 0
    l = n // 2
    m = n - l
    left = arr[:l]
    right = arr[l:]
    invs = merge_sort(left) + merge_sort(right)
    i = 0
    j = 0
    for k in range(n):
        if i < l and (j >= m or left[i] <= right[j]):
            arr[k] = left[i]
            i += 1
            invs += j
        elif j < m:
            arr[k] = right[j]
            j += 1
    return invs

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    arr = list(map(int, input().strip().split(' ')))
    print(merge_sort(arr))
