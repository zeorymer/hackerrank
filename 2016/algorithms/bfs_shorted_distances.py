from collections import deque

class Graph(object):
    def __init__(self, n):
        self.n = n
        self.edges = [set() for i in range(n)]

    def connect(self, x, y):
        self.edges[x].add(y)
        self.edges[y].add(x)

    def find_all_distances(self, s):
        distances = [-1] * (self.n - 1)
        q = deque()
        q.append(s)
        visited = {}
        visited[s] = 0
        d = 6
        while len(q) > 0:
            n = q.popleft()
            for y in self.edges[n]:
                if y not in visited:
                    visited[y] = visited[n] + 6
                    q.append(y)
                    if y < s:
                        distances[y] = visited[y]
                    if y > s:
                        distances[y-1] = visited[y]
        return distances

t = int(input())
for i in range(t):
    n,m = [int(value) for value in input().split()]
    graph = Graph(n)
    for i in range(m):
        x,y = [int(x) for x in input().split()]
        graph.connect(x-1,y-1)
    s = int(input())
    print(' '.join(map(str, graph.find_all_distances(s-1))))

