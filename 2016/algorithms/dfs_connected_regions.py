def getBiggestRegion(grid, row, col):
    def dfs(grid, x, y):
        if x < 0 or y < 0 or x >= row or y >= col:
            return 0
        if grid[x][y] == 0:
            return 0
        count = grid[x][y]
        grid[x][y] = 0
        count += dfs(grid, x + 1, y)
        count += dfs(grid, x - 1, y)
        count += dfs(grid, x + 1, y + 1)
        count += dfs(grid, x - 1, y + 1)
        count += dfs(grid, x, y + 1)
        count += dfs(grid, x, y - 1)
        count += dfs(grid, x + 1, y - 1)
        count += dfs(grid, x - 1, y - 1)

        return count

    mlen = 0
    for i in range(row):
        for j in range(col):
            l = dfs(grid, i, j)
            if l > mlen:
                mlen = l
    return mlen


n = int(input().strip())
m = int(input().strip())
grid = []
for grid_i in range(n):
    grid_t = list(map(int, input().strip().split(' ')))
    grid.append(grid_t)
print(getBiggestRegion(grid, n, m))
