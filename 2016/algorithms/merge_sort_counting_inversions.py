import copy
def count_inversions(arr):
    aux = copy.copy(arr)
    return count_invs(arr, 0, len(arr) - 1, aux)

def count_invs(arr, lo, hi, aux):
    if lo >= hi:
        return 0
    mid = lo + (hi - lo) // 2
    count = 0
    count += count_invs(aux, lo, mid, arr)
    count += count_invs(aux, mid + 1, hi, arr)
    count += merge(arr, lo, mid, hi, aux)

    return count

def merge(arr, lo, mid, hi, aux):
    count = 0
    i = lo
    j = mid + 1
    k = lo
    while i <= mid or j <= hi:
        if i > mid:
            arr[k] = aux[j]
            k += 1
            j += 1
        elif j > hi:
            arr[k] = aux[i]
            k += 1
            i += 1
        elif aux[i] <= aux[j]:
            arr[k] = aux[i]
            k += 1
            i += 1
        else:
            arr[k] = aux[j]
            k += 1
            j += 1
            count += mid + 1 - i
    return count

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    arr = list(map(int, input().strip().split(' ')))
    print(count_inversions(arr))
