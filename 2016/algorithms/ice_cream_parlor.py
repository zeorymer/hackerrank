def find_combo(arr, m):
    s = sorted(arr)
    i = 0
    j = len(s) - 1
    while i < j:
        t = s[i] + s[j]
        if t == m:
            break
        elif t > m:
            j -= 1
        else:
            i += 1
    if i >= j:
        raise Exception('Not found')
    a = None
    b = None
    for idx, x in enumerate(arr):
        if x == s[i] or x == s[j]:
            if a == None:
                a = idx
            else:
                b = idx
                return (a, b)



t = int(input().strip())
for a0 in range(t):
    m = int(input().strip())
    n = int(input().strip())
    a = list(map(int, input().strip().split(' ')))
    f = find_combo(a, m)
    print('{0} {1}'.format(f[0] + 1, f[1] + 1))

