def ransom_note(magazine, rasom):
    founds = {}
    for word in magazine:
        if word not in founds:
            founds[word] = 1
        else:
            founds[word] += 1
    for word in ransom:
        if word not in founds or founds[word] == 0:
            return False
        else:
            founds[word] -= 1
    return True


m, n = map(int, input().strip().split(' '))
magazine = input().strip().split(' ')
ransom = input().strip().split(' ')
answer = ransom_note(magazine, ransom)
if(answer):
    print("Yes")
else:
    print("No")
