from heapq import heappush, heappop

n = int(input().strip())
a = []
for i in range(0, n):
    a.append(int(input().strip()))

hi = []
lo = []
mid = None
for v in a:
    if mid != None:
        if mid > v:
            heappush(hi, mid)
            heappush(lo, -v)
        else:
            heappush(hi, v)
            heappush(lo, -mid)
        mid = None
        print("{0:0.1f}".format((hi[0] + abs(lo[0])) / 2))
    else:
        mid = v
        if len(hi) > 0 and len(lo) > 0:
            if abs(lo[0]) > mid:
                temp = mid
                mid = abs(heappop(lo))
                heappush(lo, -temp)
            if hi[0] < mid:
                temp = mid
                mid = heappop(hi)
                heappush(hi, temp)
        print("{0:0.1f}".format(mid))
