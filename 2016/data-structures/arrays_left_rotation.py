def array_left_rotation(a, n, k):
    arr = list(a)
    r = k % n
    return arr[r:] + arr[:r]


n, k = map(int, input().strip().split(' '))
a = map(int, input().strip().split(' '))
answer = array_left_rotation(a, n, k)
print(*answer, sep=' ')
