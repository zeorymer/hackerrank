def is_matched(expression):
    s = []
    for token in expression:
        if token == '{' or token == '(' or token == '[':
            s.append(token)
        else:
            if len(s) == 0: # potential empty stack
                return False
            match = s.pop()
            if token == '}' and match != '{':
                return False
            if token == ')' and match != '(':
                return False
            if token == ']' and match != '[':
                return False

    return False if len(s) > 0 else True # poptential remaining tokens on stack

t = int(input().strip())
for a0 in range(t):
    expression = input().strip()
    if is_matched(expression) == True:
        print("YES")
    else:
        print("NO")
