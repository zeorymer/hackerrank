from collections import defaultdict

# the whole stupid split suffix functionality
# is only here because very rare long words will screw memory
# consumption otherwise

class Node(object):
    def __init__(self):
        self.val = ''
        self.children = defaultdict(Node)
        self.num_usage = 0

    @property
    def is_whole_suffix(self):
        return self.num_usage == 1

    @property
    def children_suffix(self):
        assert self.is_whole_suffix         # meaningful only for whole suffixes
        return self.val[1:]

    def split_suffix(self):
        assert self.is_whole_suffix         # meaningful only for whole suffixes
        child = self.children[self.val[1]]
        child.val = self.val[1:]
        child.num_usage += 1

    def add(self, char):
        if self.is_whole_suffix:            # preserve memory by concatinating
            self.val += char                # characters in suffix  if the word
            return self                     # is used only once

        child = self.children[char]
        if child.is_whole_suffix and len(child.val) > 1:
            child.split_suffix()

        child.val = char
        child.num_usage += 1
        return child

    def get(self, char):
        assert self.has(char)               # because defaultdict is used
        return self.children[char]

    def has(self, char):
        return char in self.children

    def has_same_suffix(self, rest_partial):
        assert self.is_whole_suffix         # meaningful only for whole suffixes
        return self.children_suffix[:len(rest_partial)] == rest_partial

class Trie(object):
    def __init__(self):
        self.root = Node()

    def add(self, word):
        node = self.root
        for char in word:
            node = node.add(char)

    def find(self, partial):
        node = self.root
        for i, char in enumerate(partial):
            if node.is_whole_suffix:
                rest_partial = partial[i:]
                return int(node.has_same_suffix(rest_partial))
            if not node.has(char):
                return 0
            node = node.get(char)
        return node.num_usage

n = int(input().strip())
trie = Trie()
for a0 in range(n):
    op, contact = input().strip().split(' ')
    if   op == 'add':
        trie.add(contact)
    elif op == 'find':
        print(trie.find(contact))
