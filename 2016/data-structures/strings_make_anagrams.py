def number_needed(a, b):
    founds = {}
    for ch in a:
        if ch not in founds:
            founds[ch] = 1
        else:
            founds[ch] += 1
    for ch in b:
        if ch not in founds:
            founds[ch] = -1
        else:
            founds[ch] -= 1
    diff = 0
    for _, counts in founds.items():
        diff += abs(counts)
    return diff

a = input().strip()
b = input().strip()

print(number_needed(a, b))
