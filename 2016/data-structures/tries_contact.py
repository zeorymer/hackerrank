class Node(object):
    def __init__(self):
        self.count = 0
        self.children = [None] * 26

def insert(node, s):
    if not node:
        return

    curr = node
    for ch in s:
        index = ord(ch) - ord('a')
        if not curr.children[index]:
            curr.children[index] = Node()
        curr.children[index].count += 1
        curr = curr.children[index]

def match(node, s):
    if not node:
        return -1

    curr = node
    for ch in s:
        index = ord(ch) - ord('a')
        if not curr.children[index]:
            return 0
        curr = curr.children[index]
    return curr.count


n = int(input().strip().lower())
root = Node()
for a0 in range(n):
    op, contact = input().strip().split(' ')
    if op == 'add':
        insert(root, contact.lower())
    if op == 'find':
        print(match(root, contact))
