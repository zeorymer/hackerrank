""" Node is defined as
class node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
"""

def check_binary_search_tree_(root):
    def check(node, lo, hi):
        if not node:
            return True
        if lo is not None and node.data <= lo:
            return False
        if hi is not None and node.data >= hi:
            return False
        return check(node.left, lo, node.data) and check(node.right, node.data, hi)
    return check(root, None, None)
