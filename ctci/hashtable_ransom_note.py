#!/bin/python3
from collections import Counter

# Complete the checkMagazine function below.
def check_magazine(magazine, note):
    dictionary = Counter(magazine)
    for word in note:
        if not dictionary[word]:
            print('No')
            return False
        dictionary[word] -= 1
    print('Yes')
    return True

if __name__ == '__main__':
    mn = input().split()

    m = int(mn[0])

    n = int(mn[1])

    magazine = input().rstrip().split()

    note = input().rstrip().split()

    check_magazine(magazine, note)
