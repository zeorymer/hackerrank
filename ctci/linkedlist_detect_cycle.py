"""
Detect a cycle in a linked list. Note that the head pointer may be 'None' if the list is empty.

A Node is defined as:

    class Node(object):
        def __init__(self, data = None, next_node = None):
            self.data = data
            self.next = next_node
"""


def has_cycle(head):
    if not head:
        return True
    fast = slow = head
    while fast and slow:
        if not fast.next or not fast.next.next:
            return False
        fast = fast.next.next
        slow = slow.next

        if fast == slow:
            return True