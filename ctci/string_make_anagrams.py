#!/bin/python3

from collections import Counter
import os

# Complete the makeAnagram function below.
def make_anagram(a, b):
    count_a = Counter(a)
    count_b = Counter(b)
    s = 0
    for ch, count in count_a.items():
        if ch not in count_b:
            s += count
        else:
            s += abs(count - count_b[ch])

    for ch, count in count_b.items():
        if ch in count_a:
            continue
        s += count

    return s


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = input()

    b = input()

    res = make_anagram(a, b)

    fptr.write(str(res) + '\n')

    fptr.close()
